$(document).ready(function () {
    $('#post').click(function (e) {
        e.preventDefault();
        var name = $('#name').val();
        var street = $('#street').val();
        var postalcode = $('#postalcode').val();
        var city = $('#city').val();
        var province = $('#province').val();
        var rooms = $('#rooms').val();
        var latitude = $('#latitude').val();
        var longitude = $('#longitude').val();
        var price = $('#price').val();
        var token = $('input[name=csrfmiddlewaretoken]').val();

        if (name == '') {
            $.notify("Please enter name", "warn");
            return false;
        }
        else if (street == '') {
            $.notify("Please enter street", "warn");
            return false;
        }
        else if (postalcode == '') {
            $.notify("Please enter postalcode", "warn");
            return false;
        }
        else if (city == '') {
            $.notify("Please enter city", "warn");
            return false;
        }
        else if (province == '') {
            $.notify("Please enter province", "warn");
            return false;
        } else if (rooms == '') {
            $.notify("Please enter rooms numbers", "warn");
            return false;
        } else if (latitude == '') {
            $.notify("Please enter latitude", "warn");
            return false;
        } else if (longitude == '') {
            $.notify("Please enter longitude", "warn");
            return false;
        } else if (price == '') {
            $.notify("Please enter price", "warn");
            return false;
        }
        else {
            $.ajax({
                type: "post",
                url: base_url + "/create_post/",
                data: {
                    name: name,
                    street: street,
                    postalcode: postalcode,
                    city: city,
                    province: province,
                    rooms: rooms,
                    latitude: latitude,
                    longitude: longitude,
                    price: price,
                    csrfmiddlewaretoken: token
                },
                success: function (data) {
                    if (data.status) {
                        $('#postModal').modal('hide');
                        $.notify("Successfully Created !!", "success");
                        resetPost();
                        location.reload();
                    } else {
                        $.notify("Email address already used!!", "error");
                    }
                }
            });
        }
    });
    $('#all_post').click(function (e) {
        e.preventDefault();
        $.ajax({
            type: "get",
            url: base_url + "/get_all_post_by_user/",
            success: function (data) {
                for (var i = 0; i < data.length; i++) {
                    console.log()

                    var html = '<div class="form-group" style="border-top: 1px solid white;/*! border-left: none; *//*! border-right: none; */padding: 5px 0;margin-bottom: 0;">'
                    html += '<label for="inputEmail3"style="/*! float: left; */text-align: left;padding-left: 0;padding-right: 0;"class="col-sm-3 control-label">' + data[i].name + '</label>'
                    html += '         <div style="padding: 5px;" class="col-sm-7">'
                    html += '             <span>' + data[i].street + ',' + data[i].postalcode + ',' + data[i].province + ',' + data[i].city + ',' + data[i].country + '</span></br>'
                    html += '             <span><b>No of rooms in apartments :</b> ' + data[i].rooms + '<br/><b> Monthly price :</b>' + data[i].price + '</span>'
                    html += '         </div>'
                    html += '         <div style="float: right;padding: 0;" class="col-sm-2">'
                    html += '             <button class="btn btn-default vankober-btn postDelete" data-id="' + data[i].id + '">delete'
                    html += '             </button>'
                    html += '         </div>'
                    html += '     </div>'
                }
                $('#postlist').html(html);
            }
        });

    });
    $('body').on('click', '.postDelete', function () {
        var id = $(this).data('id');
        var token = $('input[name=csrfmiddlewaretoken]').val();
        $.ajax({
            type: "Post",
            data: {
                id: id,
                csrfmiddlewaretoken: token
            },
            url: base_url + "/delete_post/",
            success: function (data) {
                if (data.status) {
                    $('.postDelete').parent().parent().remove();
                    $.notify("Post Successfully Deleted !!", "success");

                }

            }
        });


    });

    $('body').on('click','#newpost',function(){
        $('#allPostsModal').modal('hide');
        resize();
        $('#postModal').modal("show");
        document.body.scroll = "yes";
    });
    function resetPost() {
        $('#name').val("");
        $('#street').val("");
        $('#postalcode').val("");
        $('#city').val("");
        $('#province').val("");
        $('#rooms').val("");
        $('#latitude').val("");
        $('#longitude').val("");
        $('#price').val("");
    }


});
