$(document).ready(function () {
    $('#create_account').click(function (e) {
        e.preventDefault();
        var nickname = $('#nickname').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var confirm_password = $('#confirm_password').val();
        var token = $('input[name=csrfmiddlewaretoken]').val();

        if (nickname == '') {
            $.notify("Please enter your Nickname", "warn");
            return false;
        }
        else if (email == '') {
            $.notify("Please enter your email", "warn");
            return false;
        }
        else if (password == '') {
            $.notify("Please enter your password", "warn");
            return false;
        }
        else if (confirm_password == '') {
            $.notify("Please enter your confirm password", "warn");
            return false;
        }
        else if (confirm_password != password) {
            $.notify("Password and confirm password doesnt match", "warn");
            return false;
        }
        else if (!validateEmail(email)) {
            $.notify('Invalid Email Address', "warn");
            return false;
        }
        else {
            $.ajax({
                type: "post",
                url: base_url + "/create_account/",
                data: {
                    nickname: nickname,
                    email: email,
                    password: password,
                    csrfmiddlewaretoken: token
                },
                success: function (data) {
                    if (data.status) {
                        $('#loginModal').modal('hide');
                        $.notify("Successfully Registered !!", "success");
                        reset();
                    } else {
                        $.notify("Email address already used!!", "error");
                    }
                }
            });
        }
    });

    $('#login').click(function (e) {
        e.preventDefault();
        var email = $('#login_email').val();
        var password = $('#login_password').val();
        var token = $('input[name=csrfmiddlewaretoken]').val();

        if (email == '') {
            $.notify("Please enter your email", "warn");
            return false;
        }
        else if (password == '') {
            $.notify("Please enter your password", "warn");
            return false;
        }

        else {
            $.ajax({
                type: "post",
                url: base_url + "/login/",
                data: {
                    email: email,
                    password: password,
                    csrfmiddlewaretoken: token
                },
                success: function (data) {
                    if (data.status) {
                        $('#loginModal').modal('hide');
                        $.notify("Successfully Login !!", "success");
                        location.reload();
                    } else {
                        $.notify("login Failed!!", "error");
                    }
                }
            });
        }
    });
    $('#logout').click(function (e) {

        var token = $('input[name=csrfmiddlewaretoken]').val();
        $.ajax({
            type: "post",
            url: base_url + "/logout/",
            data: {
                    csrfmiddlewaretoken: token
                },
            success: function (data) {
                if (data.status) {
                    $.notify("Successfully Logged out !!", "success");
                    location.reload();
                } else {
                    $.notify("logout Failed!!", "error");
                }
            }
        });
    });
    function reset() {
        $('#nickname').val("");
        $('#email').val("");
        $('#password').val("");
        $('#confirm_password').val("");
    }

    function validateEmail(emailField) {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(emailField) == false) {

            return false;
        }

        return true;

    }


});
