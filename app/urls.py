from django.conf.urls import url
from .views import common
urlpatterns = [
    url(r'^$',common.Home.as_view(), name='index'),
    url(r'^get_all_markers/$',common.getMarkers, name='get-all-markers'),
    url(r'^create_account/$',common.create_account, name='create-account'),
    url(r'^create_post/$',common.create_post, name='create-post'),
    url(r'^get_all_post_by_user/$',common.get_all_post_by_user, name='get-all-post-by-user'),
    url(r'^delete_post/$',common.delete_post, name='delete-post'),
    url(r'^login/$',common.login, name='login'),
    url(r'^logout/$',common.logout, name='logout')
    # url(r'^detail/(?P<pk>[0-9]+)/$', views.detail, name='detail'),
    # url(r'^post-comment/$', views.post_comment, name='post-comment'),
    # url(r'^post-rating/$', views.post_rating, name='post-rating')
]
