from django.views.generic import TemplateView
from django.http import HttpResponse
from django.shortcuts import render, redirect
from adminapp.models import Accounts,Users
import json
import datetime
import hashlib


class Home(TemplateView):
    def get(self, request):
        today = datetime.datetime.now()
        dates = [today + datetime.timedelta(days=i) for i in range(0 - today.weekday(), 7 - today.weekday())]


        data=[]
        for date in dates:
            toda = date.date()
            tomorrow = toda + datetime.timedelta(1)
            today_start = datetime.datetime.combine(toda, datetime.time())
            today_end = datetime.datetime.combine(tomorrow, datetime.time())
            count = Accounts.objects.filter(type="event",event_date__lte=today_end, event_end_date__gte=today_start).count()
            obj={
                'date':date,
                'count':count
            }
            data.append(obj)



        context = {
            'data': data
        }
        return render(request, 'home/index.html', context)


def getMarkers(request):
        type = request.GET.get('type')
        toTime = request.GET.get('toTime')
        fromTime = request.GET.get('fromTime')
        toDateTime = request.GET.get('toDateTime')
        fromDateTime = request.GET.get('fromDateTime')
        markers = Accounts.objects.all()
        if type:
            markers = Accounts.objects.filter(category__in=json.loads(type))
            if fromTime and toTime:
                markers = Accounts.objects.filter(category__in=json.loads(type),opening__lte=toTime,closing__gte=fromTime)
                print(markers.query)
            if toDateTime and fromDateTime:
                markers = Accounts.objects.filter(category__in=json.loads(type),event_date__lte=toDateTime,event_end_date__gte=fromDateTime)
                print(markers.query)




        response_data = []
        for marker in markers:
            response_data.append(marker.as_dict())

        return HttpResponse(json.dumps(response_data), content_type="application/json")

def create_account(request):
    nickname = request.POST.get('nickname')
    email = request.POST.get('email')
    password = request.POST.get('password')
    encrypted =str(hashlib.md5(password).hexdigest())

    user = Users(nickname=nickname,email=email,password=encrypted)
    try:
      user.save()
      response_data={
        'user':user.as_dict(),
        'status':True
      }
    except:
        response_data={
            'status':False
        }
        pass

    return HttpResponse(json.dumps(response_data), content_type="application/json")

def create_post(request):
    name = request.POST.get('name')
    street = request.POST.get('street')
    city = request.POST.get('city')
    postalcode = request.POST.get('postalcode')
    province = request.POST.get('province')
    rooms = request.POST.get('rooms')
    latitude = request.POST.get('latitude')
    longitude = request.POST.get('longitude')
    price = request.POST.get('price')


    Account = Accounts(name=name,country='canada',created_by_id=request.session['user_id'],category='rents',street=street,city=city,postalcode=postalcode,province=province,rooms=rooms,lat=latitude,lon=longitude,price=price)
    try:
      Account.save()
      response_data={
        'account':Account.as_dict(),
        'status':True
      }
    except:
        response_data={
            'status':False
        }
        pass

    return HttpResponse(json.dumps(response_data), content_type="application/json")

def get_all_post_by_user(request):
    accounts = Accounts.objects.filter(created_by_id=request.session['user_id'])

    account_list=[]
    for account in accounts:
        account_list.append(account.as_dict())

    return HttpResponse(json.dumps(account_list), content_type="application/json")

def delete_post(request):
    id = request.POST.get('id')
    Accounts.objects.filter(id=id).delete()
    response_data ={
        'status': True
    }
    return HttpResponse(json.dumps(response_data), content_type="application/json")

def login(request):
    email = request.POST.get('email')
    password = request.POST.get('password')
    encrypted =str(hashlib.md5(password).hexdigest())

    user = Users.objects.filter(email=email,password=encrypted)
    if user.exists():
        request.session['user_id'] =user[0].id
        request.session['user_email'] =user[0].email
        request.session['user_nickname'] =user[0].nickname
        response_data={
            'status':True
        }
    else:
        response_data={
            'status':False
        }


    return HttpResponse(json.dumps(response_data), content_type="application/json")

def logout(request):
    del request.session['user_id']
    del request.session['user_email']
    response_data={
            'status':True
    }

    return HttpResponse(json.dumps(response_data), content_type="application/json")

