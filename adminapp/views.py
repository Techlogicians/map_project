from django.shortcuts import render,redirect
from adminapp.models import Accounts,Admins

def index(request):
    return render(request,'login.html')

def checklogin(request):
    email = request.POST.get('email')
    password = request.POST.get('password')

    admin = Admins.objects.filter(email=email,password=password)

    if admin.exists():
        request.session['admin_id'] =admin[0].id
        request.session['admin_email'] =admin[0].email
        return redirect('dashboard')
    else:
        return redirect('login')


def logout(request):
    del request.session['admin_id']
    del request.session['admin_email']
    return redirect('login')


def dashboard(request):
    if 'admin_id' in request.session:
        return render(request,'dashboard/index.html')
    else:
        return redirect('login')


def add_resturant(request):
    if 'admin_id' in request.session:
        return render(request,'resturants/add.html')
    else:
        return redirect('login')

def save_resturant(request):
    name = request.POST.get('name')
    street = request.POST.get('street')
    province = request.POST.get('province')
    postalcode = request.POST.get('postalcode')
    city = request.POST.get('city')
    country = request.POST.get('country')
    latitude = request.POST.get('latitude')
    longitude = request.POST.get('longitude')
    opening = request.POST.get('opening')
    closing = request.POST.get('closing')

    account = Accounts(name=name,category="restaurant",opening=opening,closing=closing,street=street,province=province,postalcode=postalcode,city=city,country=country,lat=latitude,lon=longitude)
    account.save()

    return redirect('restaurant-add')

def add_tourist_attraction(request):
    if 'admin_id' in request.session:
        return render(request,'tourist_attraction/add.html')
    else:
        return redirect('login')

def save_tourist_attraction(request):
    name = request.POST.get('name')
    street = request.POST.get('street')
    province = request.POST.get('province')
    postalcode = request.POST.get('postalcode')
    city = request.POST.get('city')
    country = request.POST.get('country')
    latitude = request.POST.get('latitude')
    longitude = request.POST.get('longitude')
    opening = request.POST.get('opening')
    closing = request.POST.get('closing')

    account = Accounts(name=name,category="tourist_attraction",opening=opening,closing=closing,street=street,province=province,postalcode=postalcode,city=city,country=country,lat=latitude,lon=longitude)
    account.save()

    return redirect('tourist-attraction-add')

def add_activity(request):
    if 'admin_id' in request.session:
        return render(request,'activity/add.html')
    else:
        return redirect('login')


def save_activity(request):
    name = request.POST.get('name')
    category = request.POST.get('category')
    street = request.POST.get('street')
    province = request.POST.get('province')
    postalcode = request.POST.get('postalcode')
    city = request.POST.get('city')
    country = request.POST.get('country')
    latitude = request.POST.get('latitude')
    longitude = request.POST.get('longitude')
    opening = request.POST.get('opening')
    closing = request.POST.get('closing')

    account = Accounts(name=name,category=category,opening=opening,closing=closing,type='activity',street=street,province=province,postalcode=postalcode,city=city,country=country,lat=latitude,lon=longitude)
    account.save()

    return redirect('activity-add')

def add_event(request):
    if 'admin_id' in request.session:
        return render(request,'event/add.html')
    else:
        return redirect('login')

def save_event(request):
    name = request.POST.get('name')
    category = request.POST.get('category')
    street = request.POST.get('street')
    province = request.POST.get('province')
    postalcode = request.POST.get('postalcode')
    city = request.POST.get('city')
    country = request.POST.get('country')
    latitude = request.POST.get('latitude')
    longitude = request.POST.get('longitude')
    event_date = request.POST.get('event_date')
    event_end_date = request.POST.get('event_end_date')

    account = Accounts(name=name,category=category,type='event',event_date=event_date,event_end_date=event_end_date,street=street,province=province,postalcode=postalcode,city=city,country=country,lat=latitude,lon=longitude)
    account.save()

    return redirect('event-add')
