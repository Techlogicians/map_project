from __future__ import unicode_literals

from django.db import models

class Admins(models.Model):
    email = models.CharField(max_length=200)
    password = models.CharField(max_length=200)

    def as_dict(self):
        return dict(
            id=self.id,
            email=self.email,
            password=self.password
        )

    class Meta:
        db_table = "admins"

class Users(models.Model):
    nickname = models.CharField(max_length=200)
    email = models.CharField(max_length=200,unique=True)
    password = models.CharField(max_length=200)

    def as_dict(self):
        return dict(
            id=self.id,
            nickname=self.nickname,
            email=self.email,
            password=self.password
        )

    class Meta:
        db_table = "users"


class Accounts(models.Model):
    name = models.CharField(max_length=200)
    category = models.CharField(max_length=200)
    type = models.CharField(max_length=200,null=True)
    street = models.CharField(max_length=200)
    province = models.CharField(max_length=200)
    postalcode = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    country = models.CharField(max_length=200)
    lat = models.FloatField()
    lon = models.FloatField()
    event_date = models.DateTimeField(null=True)
    event_end_date = models.DateTimeField(null=True)
    opening = models.TimeField(null=True)
    closing = models.TimeField(null=True)
    rooms = models.IntegerField(null=True)
    price = models.FloatField(null=True)
    created_by = models.ForeignKey(Users, null=True)

    def as_dict(self):
        created_by = self.created_by.as_dict() if self.created_by != None else ''
        return dict(
            id=self.id,
            name=self.name,
            category=self.category,
            type=self.type,
            street=self.street,
            province=self.province,
            postalcode=self.postalcode,
            city=self.city,
            country=self.country,
            lat=self.lat,
            lon=self.lon,
            event_date=str(self.event_date),
            event_end_date=str(self.event_end_date),
            opening=str(self.opening),
            closing=str(self.closing),
            rooms=self.rooms,
            price=self.price,
            created_by=created_by,
        )

    class Meta:
        db_table = "accounts"