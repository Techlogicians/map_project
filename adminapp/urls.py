from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='login'),
    url(r'^check_login/$', views.checklogin, name='check-login'),
    url(r'^logout/$', views.logout, name='logout'),


    url(r'^dashboard/$', views.dashboard, name='dashboard'),

    url(r'^restaurant/add/$', views.add_resturant, name='restaurant-add'),
    url(r'^restaurant/save/$', views.save_resturant, name='restaurant-save'),
    url(r'^restaurant/list/$', views.save_resturant, name='restaurant-list'),

    url(r'^tourist-attraction/add/$', views.add_tourist_attraction, name='tourist-attraction-add'),
    url(r'^tourist-attraction/save/$', views.save_tourist_attraction, name='tourist-attraction-save'),
    url(r'^tourist-attraction/list/$', views.save_resturant, name='tourist-attraction-list'),

    url(r'^activity/add/$', views.add_activity, name='activity-add'),
    url(r'^activity/save/$', views.save_activity, name='activity-save'),
    url(r'^activity/list/$', views.save_activity, name='activity-list'),

    url(r'^event/add/$', views.add_event, name='event-add'),
    url(r'^event/save/$', views.save_event, name='event-save'),
    url(r'^event/list/$', views.save_event, name='event-list'),

    # url(r'^detail/(?P<pk>[0-9]+)/$', views.detail, name='detail'),
    # url(r'^post-comment/$', views.post_comment, name='post-comment'),
    # url(r'^post-rating/$', views.post_rating, name='post-rating')
]
